//
//  Shots.h
//  DesafioCS
//
//  Created by mac on 01/08/15.
//  Copyright (c) 2015 Junqueira Pinheiro. All rights reserved.
//

#import "Mantle.h"
#import "MTLModel.h"

@interface Player : MTLModel <MTLJSONSerializing>

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *avatar_url;
@property (nonatomic,strong) NSString *twitter_screen_name;

@end
