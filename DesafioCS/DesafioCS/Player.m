//
//  Shots.m
//  DesafioCS
//
//  Created by mac on 01/08/15.
//  Copyright (c) 2015 Junqueira Pinheiro. All rights reserved.
//

#import "Player.h"

@implementation Player

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name": @"name",
             @"avatar_url": @"avatar_url",
             @"twitter_screen_name": @"twitter_screen_name",
             };
}

@end
