//
//  Shot.m
//  DesafioCS
//
//  Created by mac on 01/08/15.
//  Copyright (c) 2015 Junqueira Pinheiro. All rights reserved.
//

#import "Shot.h"
#import "Player.h"

@implementation Shot

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"title": @"title",
             @"desc": @"description",
             @"views_count": @"views_count",
             @"image_url": @"image_url",
             @"player": @"player"
             };
}

+ (NSValueTransformer *)playerJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[Player class]];
}

+ (Shot *)deserializeJSON:(NSDictionary *)json
{
    NSError *error;
    Shot *shots = [MTLJSONAdapter modelOfClass:[Shot class] fromJSONDictionary:json error:&error];
    if (error) {
        NSLog(@"Erro ao converter: %@", error);
        return nil;
    }
    
    return shots;
}


@end
