//
//  main.m
//  DesafioCS
//
//  Created by mac on 01/08/15.
//  Copyright (c) 2015 Junqueira Pinheiro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
