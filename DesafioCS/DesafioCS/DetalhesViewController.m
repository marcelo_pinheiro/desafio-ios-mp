//
//  DetalhesViewController.m
//  DesafioCS
//
//  Created by mac on 01/08/15.
//  Copyright (c) 2015 Junqueira Pinheiro. All rights reserved.
//

#import "DetalhesViewController.h"
#import "UIKit+AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import <Social/Social.h>

@interface DetalhesViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *svConteudo;
@property (weak, nonatomic) IBOutlet UIImageView *imgDetalhes;
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblNome;
@property (weak, nonatomic) IBOutlet UILabel *lblTitulo;
@property (weak, nonatomic) IBOutlet UILabel *lblViews;
@property (weak, nonatomic) IBOutlet UITextView *txtDescricao;
@property (weak, nonatomic) SLComposeViewController *fbViewController;
@property (weak, nonatomic) SLComposeViewController *twitterViewController;

@end

@implementation DetalhesViewController

#pragma mark - compartilhamento

-(void) compartilhar {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Compartilhar" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:@"facebook", @"twitter", nil];
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        [self compartilharFacebook];
    } else if(buttonIndex == 1) {
        [self compartilharTwitter];
    }
}

-(void) compartilharFacebook {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        _fbViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [_fbViewController addImage:_imgDetalhes.image];
        [_fbViewController setInitialText:_txtDescricao.text];
        [self presentViewController:_fbViewController animated:YES completion:nil];
        
        _fbViewController.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Cancelado");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Compartilhado");
                    break;
            }
        };
    }
    else {
        UIAlertView *erro = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Não foi encontrada uma conta do Facebook.\n Por favor, configure uma conta nos Ajustes do seu iPhone e tente novamente." delegate:self cancelButtonTitle:@"Fechar" otherButtonTitles:nil];
        [erro show];
    }
}

-(void) compartilharTwitter {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        _twitterViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [_twitterViewController setInitialText:_txtDescricao.text];
        [self presentViewController:_twitterViewController animated:YES completion:nil];
        
        _twitterViewController.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Cancelado");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Compartilhado");
                    break;
            }
        };
        
    } else {
        UIAlertView *erro = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Não foi encontrada uma conta do Twitter.\n Por favor, configure uma conta nos Ajustes do seu iPhone e tente novamente." delegate:self cancelButtonTitle:@"Fechar" otherButtonTitles:nil];
        [erro show];
    }
}

#pragma mark - configuracaoes

-(void) configNavigation {
    
    UIBarButtonItem *btnCompartilhar = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(compartilhar)];
    self.navigationItem.rightBarButtonItem = btnCompartilhar;
    
    NSString *titulo = [_player valueForKey:@"twitter_screen_name"];
    
    if (titulo) {
        self.navigationItem.title = [NSString stringWithFormat:@"@%@", titulo];
    }
}

-(void) configConteudo {
    [_imgDetalhes setImageWithURL:[NSURL URLWithString:_shot.image_url]];
    
    [_imgAvatar setImageWithURL:[NSURL URLWithString:[_player valueForKey:@"avatar_url"]]];
    
    _lblTitulo.text = _shot.title;
    
    _lblNome.text = [_player valueForKey:@"name"];
    
    _lblViews.text = [NSString stringWithFormat:@"%ld views", (long)_shot.views_count];
    
    if (_shot.desc) {
        NSString *descricao = [NSString stringWithFormat:@"%@%@%@", @"<div style='font-size: 14px; font-family: HelveticaNeue; color: #026cb6;'>", _shot.desc, @"</div>"];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[descricao dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        _txtDescricao.attributedText = attributedString;
        
        [_txtDescricao sizeToFit];
        
    }
}

#pragma mark - viewDidLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configNavigation];
    [self configConteudo];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CGSize tamanho = CGSizeMake(_svConteudo.frame.size.width, _txtDescricao.frame.origin.y+_txtDescricao.frame.size.height);
    _svConteudo.contentSize = tamanho;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
