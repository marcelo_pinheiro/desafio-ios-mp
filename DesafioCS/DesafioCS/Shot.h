//
//  Shot.h
//  DesafioCS
//
//  Created by mac on 01/08/15.
//  Copyright (c) 2015 Junqueira Pinheiro. All rights reserved.
//

#import "Mantle.h"
#import "MTLModel.h"

@interface Shot : MTLModel <MTLJSONSerializing>

@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *desc;
@property (nonatomic,assign) NSInteger views_count;
@property (nonatomic,strong) NSString *image_url;
@property (nonatomic,strong) NSDictionary *player;

+ (Shot *)deserializeJSON:(NSDictionary *)json;

@end
