//
//  ViewController.m
//  DesafioCS
//
//  Created by mac on 01/08/15.
//  Copyright (c) 2015 Junqueira Pinheiro. All rights reserved.
//

#import "ViewController.h"
#import "Shot.h"
#import <AFNetworking.h>
#import "ShotTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "DetalhesViewController.h"
#import <UIRefreshControl+AFNetworking.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tabela;
@property (nonatomic, strong) NSMutableArray *shots;
@property (nonatomic, assign) NSInteger pagina;
@property (nonatomic, assign) NSInteger paginas;
@property (nonatomic, strong) UIRefreshControl *refreshShots;
@end

@implementation ViewController

#pragma mark - tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _shots.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ShotTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    Shot *conteudo = [Shot deserializeJSON:[_shots objectAtIndex:indexPath.row]];
    
    cell.lblTitulo.text = conteudo.title;
    
    cell.lblLikes.text = [NSString stringWithFormat:@"%ld", (long)conteudo.views_count];
    
    UIImage *placeholder = [UIImage imageNamed:@"placeholder"];
    [cell.imagemShot setImageWithURL:[NSURL URLWithString:conteudo.image_url] placeholderImage:placeholder];
    
    if (indexPath.row == _shots.count-1) {
        if (_pagina < _paginas) {
            _pagina++;
        } else {
            _pagina = 1;
        }
        [self buscarShots];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Shot *conteudo = [Shot deserializeJSON:[_shots objectAtIndex:indexPath.row]];
    Player *conteudoPlayer = [[_shots objectAtIndex:indexPath.row] objectForKey:@"player"];
    
    DetalhesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetalhesViewController"];
    vc.shot = conteudo;
    vc.player = conteudoPlayer;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - refresh

-(void) refresh {
    UITableViewController *tabelaViewController = [[UITableViewController alloc] init];
    tabelaViewController.tableView = _tabela;
    
    _refreshShots = [[UIRefreshControl alloc] init];
    [self.refreshShots addTarget:self action:@selector(refreshingShots) forControlEvents:UIControlEventValueChanged];
    tabelaViewController.refreshControl = _refreshShots;
    _refreshShots.tintColor = [UIColor colorWithRed:0.01 green:0.42 blue:0.71 alpha:1.0];
}

- (void)refreshingShots{
    
    _shots = [[NSMutableArray alloc] init];
    if (_pagina < _paginas) {
        _pagina++;
    } else {
        _pagina = 1;
    }
    [self buscarShots];
    [_refreshShots endRefreshing];
}

#pragma mark - buscarShots

- (void)buscarShots {
    
    NSString *strURL = [NSString stringWithFormat:@"http://api.dribbble.com/shots/popular?page=%li", (long)self.pagina];
    
    NSURL *url = [NSURL URLWithString:strURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *response = (NSDictionary *)responseObject;
        
        [_shots addObjectsFromArray:response[@"shots"]];

        [_tabela reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Erro ao carregar dados" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Fechar" otherButtonTitles:nil];
        [alertView show];
    }];
    
    [operation start];
    
}

#pragma mark - viewDidLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _shots = [[NSMutableArray alloc] init];
    _pagina = 50;
    _paginas = 50;
    
    [self refresh];
    [self buscarShots];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
