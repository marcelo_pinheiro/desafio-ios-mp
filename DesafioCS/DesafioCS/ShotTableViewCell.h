//
//  ShotTableViewCell.h
//  DesafioCS
//
//  Created by mac on 01/08/15.
//  Copyright (c) 2015 Junqueira Pinheiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShotTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagemShot;
@property (weak, nonatomic) IBOutlet UILabel *lblTitulo;
@property (weak, nonatomic) IBOutlet UILabel *lblLikes;

@end
